# Hotel-Management-Prolog
Expert hotel management system written in Prolog.

#### To start the program, simply type 'book.'

```prolog
?- book.
Hi, welcome to Aliah & Fuadah Hotel
Please end up with (.) after put your answer, thank you

Would you like to book a room?
0 Yes
1 No
|: 0.
Please enter check-in date, e.g("11-02-2017")
|: "11-02-2017".
Please enter check-out date, e.g("12-02-2017")
|: "12-02-2017".
How many person do you want to book? Counted if age >= 12.
|: 10.
How many kids do you have? put 0 if do not have.
|: 0.
What type of room do you want?
0 Single bed: +RM10
1 Twin bed: +RM20
2 Queen bed: +RM30
3 King bed: +RM40
|: 0.
Do you want breakfast?
0 Yes
1 No
|: 0.
What do we need to call you?
0 Mr
1 Mrs
2 Ms
|: 0.
What is your full name?
|: "Husein Zolkepli".
What is your email?
|: "husein.zol05@gmail.com".
What is your phone number
|: "0135956822".
What type of payment you want to use?
0 Credit Card
1 Online Banking
2 Cash
|: 0.
Choose one
0 American Express
1 Visa
2 Master Card
|: 0.
Mr Husein Zolkepli
your email is: husein.zol05@gmail.com
your contact is: 0135956822
you book for: 1 days
Total adult: 10
Total kid: 0
breakfast: yes
Total price is: RM310
Thank you, goodbye.
```

![alt text](output1.png)
![alt text](output2.png)
![alt text](output3.png)
