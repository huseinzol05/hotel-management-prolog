% Booking system only can start if user type 'book' in prolog intepreter.
book:-
    intro,
    ask(book_room, [yes, no], firsttime).
    
intro:-
    write('Hi, welcome to Aliah & Fuadah Hotel'), nl,
    write('Please end up with (.) after put your answer, thank you'), nl, nl.

% Our hotel rules
book_system(no):-
    write('Thank you, see you again'), nl,
    halt(0).
    
book_system(yes):-
    question(check_in_date),
    firstdate,
    question(check_out_date),
    seconddate,
    count_person.

count_person:-
    question(person),
    read(X), getperson(X),
    got_kid.
    
got_kid:-
    question(got_kids),
    getkids,
    select_room.
    
select_room:-
    ask(type_room, [single, twin, queen, king], selectroom).
    
want_breakfast:-
    ask(breakfast, [yes, no], breakfast).
    
select_title:-
    ask(calling_name, [mr, mrs, ms], selecttitle).
    
full_details:-
    question(name),
    read(X), assert(fullname(X)),
    question(email),
    read(Y), assert(fullemail(Y)),
    question(phone),
    read(Z), assert(fullphone(Z)),
    payment_type.

payment_type:-
    ask(type_payment, [cc, online_banking, cash], paymenttype).
    
type_of_payment(X):-
    X = cc, ask(choose_one, [american_express, visa, mastercard], noncash);
    X = online_banking, ask(choose_one, [cimb, maybank, bankislam], noncash);
    X = cash, totalprice(Y), read(C), returnbalance(Y, C), printreceipt, goodbye.
    
goodbye:-
    write('Thank you, goodbye.'), nl, halt(0).

% Questions for the knowledge base
question(book_room):-
    write('Would you like to book a room?'), nl.
    
question(check_in_date):-
    write('Please enter check-in date, e.g(\"11-02-2017\")'), nl.
    
question(check_out_date):-
    write('Please enter check-out date, e.g(\"12-02-2017\")'), nl.
    
question(person):-
    write('How many person do you want to book? Counted if age >= 12.'), nl.
    
question(got_kids):-
    write('How many kids do you have? put 0 if do not have.'), nl.
    
question(type_room):-
    write('What type of room do you want?'), nl.
    
question(breakfast):-
    write('Do you want breakfast?'), nl.
    
question(type_payment):-
    write('What type of payment you want to use?'), nl.
    
question(calling_name):-
    write('What do we need to call you?'), nl.
    
question(choose_one):-
    write('Choose one'), nl.
    
question(name):-
    write('What is your full name?'), nl.
    
question(email):-
    write('What is your email?'), nl.
    
question(phone):-
    write('What is your phone number'), nl.

answer(yes):-
    write('Yes').

answer(no):-
    write('No').
    
answer(single):-
    write('Single bed: +RM10').

answer(twin):-
    write('Twin bed: +RM20').
    
answer(queen):-
    write('Queen bed: +RM30').
    
answer(king):-
    write('King bed: +RM40').
    
answer(mr):-
    write('Mr').
    
answer(mrs):-
    write('Mrs').
    
answer(ms):-
    write('Ms').
    
answer(cc):-
    write('Credit Card').

answer(online_banking):-
    write('Online Banking').
    
answer(cash):-
    write('Cash').
    
answer(american_express):-
    write('American Express').
    
answer(visa):-
    write('Visa').
    
answer(mastercard):-
    write('Master Card').
    
answer(cimb):-
    write('CIMB Click').
    
answer(maybank):-
    write('Maybank2u').
    
answer(bankislam):-
    write('Bank Islam IB').
    
printreceipt:-
    guesttitle(X), fullname(C), fullemail(V), fullphone(B),
    firstday(A), secondday(S), personcount(D), kidcount(F),
    wantbreakfast(G), Differenceday is S - A,
    write(X), write(' '), write(C), nl,
    write('your email is: '), write(V), nl,
    write('your contact is: '), write(B), nl,
    write('you book for: '), write(Differenceday), write(' days'), nl,
    write('Total adult: '), write(D), nl,
    write('Total kid: '), write(F), nl,
    write('breakfast: '), write(G), nl.    
    
totalprice(Totalprice):-
    month(X), calculateprice(X, Y),
    firstday(Z), secondday(C), personcount(V), kidcount(B), 
    room(N), roomprice(N, M),
    wantbreakfast(A), getbreakfast(A, S),
    Differenceday is C - Z,
    Totalprice is ((Y + M) * Differenceday) + ((V * 10 * S) + (B * 5 * S) * Differenceday),
    write('Total price is: RM'), write(Totalprice), nl.  
    
calculateprice(X, Y):-
    X =< 4, Y is 200;
    X =< 8, Y is 100;
    X =< 12, Y is 300.
    
roomprice(X, Y):-
    X = single, Y is 10;
    X = double, Y is 20;
    X = queen, Y is 30;
    X = king, Y is 40.
    
getbreakfast(X, Y):-
    X = yes, Y is 1;
    X = no, Y is 0.
    
returnbalance(X, Y):-
    Y >= X, C is Y - X, write('Total balance is: '), write(C);
    Y < X, write('not enough money, rejecting and bbye..'), halt(0).
    
parse(0, [First|_], First).
parse(Index, [First|Rest], Response):-
    Index > 0,
    NextIndex is Index - 1,
    parse(NextIndex, Rest, Response).
 
answers([], _).
answers([First|Rest], Index):-
    write(Index), write(' '), answer(First), nl,
    NextIndex is Index + 1,
    answers(Rest, NextIndex).
  
ask(Question, Choices, Time):-
    question(Question),
    answers(Choices, 0),
    read(Index),
    parse(Index, Choices, Response),
    choice(Response, Time).
  
choice(Response, Time):-
    Time = firsttime, book_system(Response);
    Time = selectroom, assert(room(Response)), want_breakfast;
    Time = breakfast, assert(wantbreakfast(Response)), select_title;
    Time = selecttitle, assert(guesttitle(Response)), full_details;
    Time = paymenttype, type_of_payment(Response);
    Time = noncash, printreceipt, totalprice(_), goodbye.
  
firstdate:-
    read(X), atom_codes(A, X), atom_chars(A, Z),   
    correctmonth(Z, 0),
    lengthmonth(Z, 0),
    parsemonth(Z, 0, Month, Month2),
    savemonth(Month2),
    parsedate(Z, 0, Day, Day2),
    savefirstday(Day2).
    
seconddate:-
    read(X), atom_codes(A, X), atom_chars(A, Z),   
    correctmonth(Z, 0),
    lengthmonth(Z, 0),
    parsemonth(Z, 0, Month, Month2),
    parsedate(Z, 0, Day, Day2),
    savesecondday(Day2).
    
parsemonth([], _, _, _).
parsemonth([Head|Tail], X, Val, Val2):-
    X = 3, atom_number(Head, N), Val is N, Y is X + 1, parsemonth(Tail, Y, Val, Val2);
    X = 4, atom_number(Head, N), Val2 is (Val * 10) + N, Y is X + 1, parsemonth(Tail, Y, Val, Val2);
    Y is X + 1, parsemonth(Tail, Y, Val, Val2).
  
savemonth(Val):-
    assert(month(Val)).
    
checkmonth(Val):-
    (month(X), Val = X) -> write('only accept same month'), nl, retract(month(X)), book_system(yes).

lengthmonth([], _).
lengthmonth([Head|Tail], X):-
    X > 9, write('Insert correct date'), nl, book_system(yes);
    Y is X + 1, lengthmonth(Tail, Y).

correctmonth([], _).
correctmonth([Head|Tail], X):-
    X = 3, write('Insert correct date'), nl, book_system(yes);
    Head = '-', Y is X + 1, correctmonth(Tail, Y);
    correctmonth(Tail, X).

parsedate([], _, _, _).
parsedate([Head|Tail], X, Val, Val2):-
    X = 0, atom_number(Head, N), Val is N, Y is X + 1, parsedate(Tail, Y, Val, Val2);
    X = 1, atom_number(Head, N), Val2 is (Val * 10) + N, Y is X + 1, parsedate(Tail, Y, Val, Val2);
    Y is X + 1, parsedate(Tail, Y, Val, Val2).
    
savefirstday(Val):-
    assert(firstday(Val)).

savesecondday(Val):-
    (firstday(X), Val =< X) -> write('insert correct date'), retract(month(X)), retract(firstday(X)), book_system(yes);
    assert(secondday(Val)).
    
getkids:-
    read(X), assert(kidcount(X)).
    
getperson(X):-
    X =< 0, write('Incorrect insert'), count_person;
    assert(personcount(X)).